datatype typ = VAR of string | INT | BOOL | ARROW of typ * typ

exception UnboundID

type env = (string -> typ)

fun emptyenv (x : string) : typ = raise UnboundID

fun update E (x : string) (ty : typ) y = if x = y then ty else E y

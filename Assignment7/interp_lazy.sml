(* Eric Schultz 5/27/2015 *)

use "parse.sml";

datatype result =
    RES_ERROR of string
  | RES_NUM   of int
  | RES_BOOL  of bool
  | RES_SUCC
  | RES_PRED
  | RES_ISZERO
  | RES_FUN   of (string * term);

exception not_found;

datatype env = Env of (string * result) list;

fun new_env() = Env(nil);
fun extend_env (Env(oldenv), id, value) = Env( (id, value):: oldenv);
fun extend_env_all (Env(oldenv), id_value_list) = Env(id_value_list @ oldenv);
fun lookup_env (Env(nil), id) = (print("Free Var!! "^id); raise not_found)
   |lookup_env (Env((id1,value1)::b), id) =  
        if (id1 = id) 
        then value1
	    else lookup_env(Env(b), id) ;

fun interp (exp, env) = 

  case exp of
    AST_ERROR s                 => RES_ERROR s
  | AST_NUM x                   => RES_NUM x
  | AST_BOOL b                  => RES_BOOL b
  | AST_SUCC                    => RES_SUCC
  | AST_PRED                    => RES_PRED
  | AST_ISZERO                  => RES_ISZERO
  | AST_IF (exp1, exp2, exp3)   =>
      (case (interp (exp1, env)) of
          RES_BOOL b => if b then (interp (exp2, env)) else (interp (exp3, env))
        | _ => RES_ERROR "if requires boolean argument")
  | AST_APP (exp1, exp2)        =>
      let val function = interp (exp1, env)
          val argument = interp (exp2, env)
      in
          case function of
              RES_ISZERO           => 
                (case argument of
                    RES_NUM 0 => RES_BOOL true
                  | RES_NUM _ => RES_BOOL false
                  | _         => RES_ERROR "ISZERO requires an int argument")
            | RES_PRED             => 
                (case argument of
                    RES_NUM 0 => RES_NUM 0
                  | RES_NUM n => RES_NUM (n - 1)
                  | _         => RES_ERROR "PRED requires an int argument")
            | RES_SUCC             => 
                (case argument of
                    RES_NUM n => RES_NUM (n + 1)
                  | _         => RES_ERROR "SUCC requires an int argument")
            | RES_FUN (str, var) => interp (var, env)
            | _                  => RES_ERROR "The function you tried to call does not exist"
      end
  | AST_ID (name)               => RES_ERROR "Bad variable!"
  | AST_FUN (var, exp)          => interp (exp, env)


(*  Once you have defined interp, you can try out simple examples by
      interp (parsestr "succ (succ 7)"), new_env());
    and you can try out larger examples by
      interp (parsefile "your-file-here", new_env());
*)

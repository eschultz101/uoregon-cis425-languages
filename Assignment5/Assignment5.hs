-- Eric Schultz
-- 5/11/15
-- CIS 425 Assignment 5

-- Problem 1
-- a
ones :: [Int]
ones		= 1 : ones
-- b
intList :: Int -> [Int]
intList	n	= n : intList (n + 1)
-- c
takeN :: Int -> [Int] -> [Int]
takeN n	(x:xs)	= x takeN (n - 1) xs

-- Problem 2
-- a
evens :: [Int]
evens = [x*2 | x <- [1..]]

odds :: [Int]
odds = [x*2 - 1 | x <- [1..]]

-- b
merge :: [Int] -> [Int] -> [Int]
merge [] [] = []
merge (x:first) (y:second) 
	| x <= y = x:merge first (y:second)
	| x > y = y:merge (x:first) second
-- The call merge evens odds will never terminate. However, length(merge evens odds) will terminate,
-- due to the lazy nature of the language and the ".." operator.

-- c
-- i
cubes :: [Int]
cubes = [x ^ 3 | x <- [1..]]
-- ii
expthree :: [Int]
expthree = [3 ^ x | x <- [1..]]
-- iii
count :: [Int]
count = [0,1..]
squares :: [Int]
squares = [x ^ 2 | [0,1..]]
merge count squares
-- iv
negatives :: [Int]
negatives = [-1 * x | x <- [1..]]

-- Problem 3
-- a
sift :: Int -> [Int] -> [Int]
sift p [] = []
sift p (n : ns)
	| n > p = n : sift p ns
	| otherwise = sift p ns

-- b
sieve :: [Int] -> [Int]
sieve [] = []
sieve (p : ns) = p : sieve (sift xs [p,p+p..])

-- c
primes :: [Int]
primes = sieve (intList 2)

-- Problem 4
-- 7.2
fun mult(a, b) = 
	while(!a >= 0) do (
		result = a * b
		a := !a - 1
	)

-- 7.4
-- a
-- The pass-by-value method and pass-by-value-result methods will make c = a^a after the call.
-- Pass-by-reference will make c = a^a during the call.

-- b
-- If aliased, all three methods will make c = a^a after the call. If c = a^a after the call, it does not
-- necessarily mean that the correct calculation was performed. It could be a convenient error due to
-- the exponent and the base being the same value.

-- 7.6
val x = ref 0;
fun p(y' : int ref) = 
	!y' := 1;
	!x := 0;
p(x);

-- 7.7
-- a
-- 1
-- b
-- 4
-- c
-- 3

-- 7.8
-- a
-- Under static scoping, the value of x + f(x) is 14. On line 5, x is 7, on 4 it is also 7, and on 2 it is 2.
-- b
-- Under dynamic scoping, the value of x + f(x) is also 14. On all lines x = 7 due to the nature of dynamic scoping.

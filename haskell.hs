-- Examples of function and type definitions in Haskell

f x y = x + y	-- returned values are x and y (f x y)

map f [] = []
map f (x:xs) = f x : map f xs
-- types are uppercase at the beginning, "bool" would be a variable
filter :: (a -> Bool) -> [a] -> [a]	-- list of a, all lists are [] around some variable type
filter f [] = []
filter f (x:xs) = if f x then x : filter f xs else filter f xs -- this is equal to the def below
filter f (x:xs) | f x = x : filter f xs -- indentation, rather than semicolons tell the compiler
				| otherwise = xs		-- that the definition continues
				where xs = filter f xs

f x = let y = x + x -- this def and one below are equivelant
	  in y * y

f x = y * y
	  where y = x + x

-- everything in Haskell is lazy: evaluated as late as possible, if at all
-- the heck is a Thunk?
data List a
	= Nil | Cons a (List a) -- a list is either empty or Cons'd to another list

data Tree a = 
	Lead a
	| Node (Tree a) (Tree a)

map f xs = case xs of
	[] -> []
	x:xs' -> f x map f xs'

map f xs = [f x | x <- xs] -- wut

-- [1..5] -- list of naturals from 1 to 5
-- [1...] -- list of naturals from 1 to infinity (only works because lazy evaluation)

g z = f -- g is evaluated first (and therefore gets a stack frame), f 3rd
	where f x = \y -> x + y + z -- \y 4th, x + y + z 5th. 
g 1 2 3 -- 2nd
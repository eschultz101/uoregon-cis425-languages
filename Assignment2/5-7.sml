(* 	Eric Schultz
 	CIS 425
 	4/15/15
 	Assignment 2 #4 
*)

(* part a *)

(* The addition may not work as intended because x.i may or may not be assigned a value, 
and x.s also may or may not be assigned a value. The error will occur during runtime in 
this C program. *)

(* part b *)

datatype IntString = tag_int of int | tag_str of string;

val x = if true then tag_int(3) else tag_str(“here, fido”);

let val tag_int (m) = x in m + 5 end;

(* ML will not allow the same bug to occur. The message "illegal token" is output, and it 
helps by preventing the programmer from leaving a variable valueless. The datatypes in the 
if then else statement must all agree. *)

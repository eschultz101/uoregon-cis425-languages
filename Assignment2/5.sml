(*	Eric Schultz
	CIS 425
	4/15/15
	Assignment 2 #5
*)

datatype Seq = Nil | Cons of int * (unit -> Seq)

fun head (Cons (x, _)) = x;

fun tail (Cons (_, xs)) = xs ();

(* part a *)

val ones = let fun f() = Cons(1, f) in f() end;

(* part b *)

val ilist = let fun intList(n)() = Cons(n, intList(n+1)) in intList(0)() end;

(* part c *)

fun takeN x list = Cons(x, list(x));

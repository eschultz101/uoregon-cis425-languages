(* 	Eric Schultz
 	CIS 425
 	4/15/15
 	Assignment 2 #2
*)

datatype 'a tree = LEAF of 'a | NODE of 'a tree * 'a tree;

fun reduce (oper : 'a * 'a -> 'a) (t : 'a tree) : 'a =
    case t of
        LEAF x => x
      | NODE (t1,t2) => oper (reduce oper t1, reduce oper t2);

(* This function reduce accepts a tuple (NODE) and a tree and returns some value 'a. 'a is determined 
by repeatedly reducing the nodes of the tree into leaves and evaluating them with the oper function. *)

(* 	Eric Schultz
 	CIS 425
 	4/15/15
 	Assignment 2 #3 
*)

(* part a *)

fun Curry f = fn x => fn y => f(x,y);

fun UnCurry g = fn (x,y) => g x y;

(* part b *)
(* Here the currying and uncurrying functions are inverses of each other. Curry will take any 
given function and convert it into a function that evaluates differently from an uncurried one; 
curried functions take a single argument regardless of the number that are actually listed in 
the declaration of the function. Uncurried functions are more traditional in that they will 
take as many as are listed. Due to the nature of ML, we can write functions that will convert 
between the two. *)

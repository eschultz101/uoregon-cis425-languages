(* 	Eric Schultz
 	CIS 425
 	4/15/15
 	Assignment 2 #1 
*)

(* part a *)

datatype 'a tree = LEAF of 'a | NODE of 'a tree * 'a tree;

fun maptree (f : 'a -> 'b) (t : 'a tree) : 'b tree = 
	case t of
		 LEAF x => LEAF (f x)
		| NODE (t1, t2) => NODE(maptree f t1, maptree f t2);

(* The function maptree accepts as arguments a function (which changes variables from some 'a to some 'b) 
and a tree, and returns a modified tree. The maptree function handles cases in which the tree given is a 
only a leaf or a tree (node) itself. *)

(* part b *)
(* The type ML gives my function is ('a -> 'b) -> 'a tree -> 'b tree. It is not the expected 'a to 'a type 
because variables in ML aren't redefined, new ones ('b in this case) are defined to hold the new values. *)

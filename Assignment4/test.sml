let val f = fn x => x
in (f 2, f true)
end;

(fn f => (f 2, f true))(fn x => x); (* this one throws an error, despite the fact that both do the same thing *)
(* no type checking occurs on the first value *)